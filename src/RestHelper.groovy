@Grapes([
        @Grab(group='org.codehaus.gpars', module='gpars', version='1.2.1'),
        @Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.7.1')
])
import groovy.json.JsonSlurper
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method

class RestHelper{
    /* Change these */
   static final String objectName = "ServiceOrder"
   static final String endPoint = "https://greensboroapp.timco.aero:50014"
   static final String path = "/customerOrder/524627"
   static final String headerName = "apiKey"
   static final String headerValue = "LmEX*JEJGPjNE50X_nh)COQ%Ep-wAiZPTBdxG3dgf@mY-B-if4xTa0UGOPGemSe#"
   static final String generatedFileLoc = "/tmp/legge-genertor/"

   static void main(String... args){
       /* CHANGE THESE ACCORDING */
       def http = new HTTPBuilder()
       def jsonResp
       http.ignoreSSLIssues()

       http.request( endPoint, Method.GET, ContentType.TEXT ) { req ->
           uri.path = path
           headers.'User-Agent' = "Mozilla/5.0 Firefox/3.0.4"
           headers."${headerName}" = headerValue
           headers.Accept = 'application/json'

           response.success = { resp, reader ->
               assert resp.statusLine.statusCode == 200
               def text = reader.text
               def parser = new JsonSlurper()
               jsonResp = parser.parseText(text)
           }

           response.'404' = {
               println 'Not found'
           }
       }

       createGrailsFile(jsonResp)
       createMarshaller(jsonResp)
       createJSFile(jsonResp)
   }


    static void createJSFile(jsonResponse){
        def className ="${objectName}"
        def grailsFileLoc = "${generatedFileLoc}react/${className}.js"
        new File("${generatedFileLoc}react/").mkdirs()
        def file1 = new File(grailsFileLoc)
        def grailsFileString = StringBuilder.newInstance()
        grailsFileString <<  """
'use strict';

import {Record} from 'immutable'

export default class ${className} extends Record({
"""

        jsonResponse.each { id, data ->
            addParamaters(id, grailsFileString)
        }

        grailsFileString <<
"""
}) {
    constructor(entity) {
        super({
"""
        jsonResponse.each { id, data ->
            renderConstructor(id, grailsFileString)
        }


        grailsFileString << """
\t });
}

    toString(){
        return this.title;
    }
}
"""
        file1.write(grailsFileString.toString())
    }


    static void createMarshaller(jsonResponse){
        println(jsonResponse)

        def packageInfo ="marshallers"

        def className ="${objectName}Marshaller"
        def grailsFileLoc = "${generatedFileLoc}marshaller/${className}.groovy"
        new File("${generatedFileLoc}marshaller/").mkdirs()
        def file = new File(grailsFileLoc)
        def grailsFileString = StringBuilder.newInstance()
        grailsFileString <<
"""
package ${packageInfo}

import aero.haeco.metis.${objectName}
import grails.converters.JSON

class ${className} {
    void register() {
        JSON.registerObjectMarshaller(${objectName}) { ${objectName} ${objectName.uncapitalize()} ->
            return [
"""

        jsonResponse.each { id, data ->
            addLineToMarshaller(id,objectName.uncapitalize(),grailsFileString)
        }

        grailsFileString << """
            ]
        }
    }
}

"""
        file.write(grailsFileString.toString())
    }


    static void createGrailsFile(jsonResponse){
        println(jsonResponse)

        def packageInfo ="aero.haeco.metis"
        def className ="${objectName}"
        def grailsFileLoc = "${generatedFileLoc}domains/${className}.groovy"
        new File("${generatedFileLoc}domains/").mkdirs()
        def file = new File(grailsFileLoc)
        def grailsFileString = StringBuilder.newInstance()
        grailsFileString <<
"""
package ${packageInfo}

import abstracts.RestEntity
class ${className} extends RestEntity {
"""

        jsonResponse.each { id, data ->
            addLineToGrailsFile(id,data,grailsFileString)
        }

        grailsFileString <<
"""

    static constraints = {
    }
}
"""
        file.write(grailsFileString.toString())
    }

    /* HELPERS */
    static void renderConstructor(id, grailsFileString){
        grailsFileString << "\t \t ${id}: entity.${id}"
        grailsFileString << System.getProperty("line.separator")
    }


    static void addParamaters(id, grailsFileString){
        grailsFileString << "\t ${id}: null"
        grailsFileString << System.getProperty("line.separator")
    }

    static void  addLineToMarshaller(id, textString, grailsFileString){
        grailsFileString << "\t\t\t\t ${id} : ${textString}.${id},"
        grailsFileString << System.getProperty("line.separator")
    }

    static void addLineToGrailsFile(id, data, grailsFileString){
        grailsFileString << "\t ${getGrailsData(data, id)} ${id}"
        grailsFileString << System.getProperty("line.separator")
    }

    static String getGrailsData(data, id){
        switch (data){
            case BigDecimal:
                return "Double"
            case String:
                return "String"
                if(amIDate(data)){
                    return "Date"
                }
                break
            case Integer:
                return "int"
            case ArrayList:
                // no clue
                break
            case Date:
                return "Date"
            case null:
                return "Date"
            default:
                println("HELP   ---  ${id} : ${data}")
        }
    }
}
